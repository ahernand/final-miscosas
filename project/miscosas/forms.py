from django import forms
from django.contrib.auth.models import User
from .models import  Alimentador, Perfil, Comentario

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password',)

class ModoForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ('foto','modo_pagina','tamano_letra',)

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ('cuerpo','foto',)
