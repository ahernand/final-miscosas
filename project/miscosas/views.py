from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.http import HttpResponse
from django.utils import timezone
from .models import  Alimentador, Item, Perfil, Item_votados
from .forms import UserForm, ModoForm, ComentarioForm
from .youtube_handler import YTChannel
from .last_handler import LastChannel
from .reddit_handler import RedditChannel
from .key import LASTFM_APIKEY
import urllib
import urllib.request
import operator
from django.core import serializers

def pagina_error(request):
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
    else:
        perfil = None
    context = { 'titulo': "Error 404",
                'perfil': perfil,
            }
    pagina = render(request, 'miscosas/error.html', context)
    pagina.status_code = 404
    return pagina

def best_items(request):
    best = {}
    lista_items = []
    items = Item.objects.all()
    for string in items:
        votos = Item_votados.objects.filter(item = string)
        votos_pos = votos.filter(voto = 1).count()
        votos_neg = votos.filter(voto = -1).count()
        total = votos_pos - votos_neg
        best[string.id_item] = total
    orden = sorted(best.items(), key=operator.itemgetter(1), reverse = True)
    orden = orden[0:10]
    for id, votos in orden:
        try:
            item = Item.objects.get(id_item=id)
        except Item.DoesNotExist:
            pagina_error(request)
        lista_items.append(item)
    return lista_items

def last_items(perfil):
    lista_items = []
    items = Item_votados.objects.filter(usuario = perfil.usuario)
    for item in items:
        lista_items.append(item)
    lista_items.reverse()
    lista_items = lista_items[:5]
    return lista_items

def index(request):
    items = best_items(request)
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
        ultimos_items = last_items(perfil)
    else:
        perfil = None
        ultimos_items = None
    context = { 'titulo': "Inicio",
                'alimentadores': Alimentador.objects.filter(select = True),
                'items': items,
                'ultimos_votos':ultimos_items,
                'perfil': perfil,
            }
    formato =  request.GET.get('format')
    if formato == 'xml':
        return render(request, "miscosas/index.xml", context,  content_type='text/xml')
    elif formato == 'json':
        context = [ *items,
                    *Alimentador.objects.filter(select = True),
                    *ultimos_items,
                    ]
        contenidos = serializers.serialize('json', context)
        return HttpResponse(contenidos, content_type="application/json")
    return render(request, 'miscosas/index.html', context)


def add_aliment(request):
    alimentador = request.POST["aliment"]
    id = request.POST['id']
    if alimentador == "actualizar":
        if id[0] == "r":
            alimentador = 'Reddit'
            id = id.replace('_', '/')
        elif id[0] == "U":
            alimentador = 'Youtube'
        else:
            alimentador = 'LastFM'
    try:
        if alimentador == 'Youtube':
            url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + id
            xmlStream = urllib.request.urlopen(url)
            YTChannel(xmlStream)
        elif alimentador == 'LastFM':
            id = id.replace(' ', '+')
            url = 'http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=' + \
            id + '&api_key=' + LASTFM_APIKEY
            xmlStream = urllib.request.urlopen(url)
            LastChannel(xmlStream)
        elif alimentador == 'Reddit':
            url = "https://www.reddit.com/" + id + ".rss"
            xmlStream = urllib.request.urlopen(url)
            RedditChannel(xmlStream)
    except:
        return True
    return False

def alimentadores(request):
    if request.method == "POST":
        if add_aliment(request):
            return pagina_error(request)
    try:
        alimentadores = Alimentador.objects.all()
    except Alimentador.DoesNotExist:
        return pagina_error(request)
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
    else:
        perfil = None
    context = {"alimentadores": alimentadores,
                "titulo": "Alimentadores",
                'perfil': perfil,
                }
    formato =  request.GET.get('format')
    if formato == 'xml':
        return render(request, "miscosas/alimentadores.xml", context, content_type='text/xml')
    elif formato == 'json':
        context = [ *alimentadores,
                    ]
        contenidos = serializers.serialize('json', context)
        return HttpResponse(contenidos, content_type="application/json")
    return render(request, 'miscosas/alimentadores.html', context)

def aliment(request, string):
    try:
        aliment = Alimentador.objects.get(id_channel=string)
        if request.method == "POST":
            value = request.POST['submit']
            if value == "Deseleccionar":
                aliment.select = False
            elif value == "Seleccionar":
                aliment.select = True
            aliment.save()
            if "redirect" in request.POST:
                return redirect('/miscosas/'+ request.POST['redirect'])
    except Alimentador.DoesNotExist:
        return pagina_error(request)
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
    else:
        perfil = None
    context = {"alimentador": aliment,
                "titulo": "Alimentador",
                'perfil': perfil,
                }
    formato =  request.GET.get('format')
    if formato == 'xml':
        return render(request, 'miscosas/alimentador.xml', context, content_type='text/xml')
    elif formato == 'json':
        context = [ aliment,
                    ]
        contenidos = serializers.serialize('json', context)
        return HttpResponse(contenidos, content_type="application/json")
    return render(request, 'miscosas/alimentador.html', context)

def votos(item, value, user):
    try:
        voto_user = Item_votados.objects.filter(usuario = user.id).get(item = item.id)
        if voto_user.voto == 1 and value == "dislike":
            item.votos_pos = item.votos_pos - 1
            item.votos_neg = item.votos_neg + 1
            voto_user.delete()
            voto_user = Item_votados(usuario = user, item = item, voto = -1)
            item.alimentador.puntuacion = item.alimentador.puntuacion - 2
            voto_user.save()
        elif voto_user.voto == -1 and value == "like":
            item.votos_pos = item.votos_pos + 1
            item.votos_neg = item.votos_neg - 1
            voto_user.delete()
            voto_user = Item_votados(usuario = user, item = item, voto = 1)
            item.alimentador.puntuacion = item.alimentador.puntuacion + 2
            voto_user.save()
        elif (voto_user.voto == 1 and value == "like"):
            item.votos_pos = item.votos_pos - 1
            item.alimentador.puntuacion = item.alimentador.puntuacion - 1
            voto_user.delete()
        elif (voto_user.voto == -1 and value == "dislike"):
            item.votos_neg = item.votos_neg - 1
            item.alimentador.puntuacion = item.alimentador.puntuacion + 1
            voto_user.delete()
    except Item_votados.DoesNotExist:
        voto_user = Item_votados(usuario = user, item = item, voto = 0)
        if value == "like":
            voto_user.voto = 1
            item.votos_pos = item.votos_pos + 1
            item.alimentador.puntuacion = item.alimentador.puntuacion + 1
        elif value == "dislike":
            voto_user.voto = -1
            item.votos_neg = item.votos_neg + 1
            item.alimentador.puntuacion = item.alimentador.puntuacion - 1
        voto_user.save()
    item.save()
    item.alimentador.save()

def add_new_comment(request, string, item):
    try:
        user = User.objects.get(username = request.user.username)
        perfil = Perfil.objects.get(usuario=request.user)
    except Perfil.DoesNotExist:
        return pagina_error(request)
    form = ComentarioForm(request.POST, request.FILES)
    if form.is_valid():
        comentario = form.save(commit=False)
        comentario.usuario = perfil
        comentario.item = item
        if "foto" in request.FILES:
            comentario.foto = request.FILES['foto']
        comentario.fecha = timezone.now()
        comentario.save()

def item(request, string):
    item = get_object_or_404(Item, id_item=string)
    if request.method == "POST":
        if "comment" in request.POST:
            add_new_comment(request, string, item)
        else:
            value = request.POST['submit']
            votos(item, value, request.user)
            if "redirect" in request.POST:
                return redirect('/miscosas/'+ request.POST['redirect'])

    form_c =  ComentarioForm()
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
        try:
            item_votado = Item_votados.objects.get(usuario = request.user, item = item)
        except:
            item_votado = None
    else:
        perfil = None
        item_votado = None
    context = {"item": item,
                "titulo": "Item",
                'perfil': perfil,
                "form_c": form_c,
                'item_votado': item_votado,
                }
    formato =  request.GET.get('format')
    comment = request.GET.get('comment')
    if formato == 'xml':
        if comment == 'true':
            return render(request, 'miscosas/item_comment.xml', context, content_type='text/xml')
        else:
            return render(request, 'miscosas/item.xml', context, content_type='text/xml')
    elif formato == 'json':
        if item_votado != None:
            context = [ item,
                        item_votado
                        ]
        else:
            context = [ item
            ]
        contenidos = serializers.serialize('json', context)
        return HttpResponse(contenidos, content_type="application/json")
    return render(request, 'miscosas/item.html', context)



def users_view(request):
    perfiles = Perfil.objects.all()
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
    else:
        perfil = None
    context = {'titulo': "Usuarios",
               'perfiles': perfiles,
               'perfil': perfil,
            }
    formato =  request.GET.get('format')
    if formato == 'xml':
        return render(request, 'miscosas/usuarios.xml', context, content_type='text/xml')
    elif formato == 'json':
        context = [ *perfiles,
                    ]
        contenidos = serializers.serialize('json', context)
        return HttpResponse(contenidos, content_type="application/json")
    return render(request, 'miscosas/usuarios.html', context)


def one_user(request, string):
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
    else:
        perfil = None
    try:
        user = User.objects.get(username=string)
        usuario = Perfil.objects.get(usuario=user)
    except Perfil.DoesNotExist:
        return pagina_error(request)
    if request.method == 'POST':
        form = ModoForm(request.POST, request.FILES)
        if form.is_valid():
            usuario = Perfil.objects.get(usuario=user)
            if "foto" in request.FILES:
                usuario.foto = request.FILES['foto']
            usuario.modo_pagina = request.POST['modo_pagina']
            usuario.tamano_letra = request.POST['tamano_letra']
            usuario.save()
        else:
            return pagina_error(request)
    form_m = ModoForm()
    context = { 'titulo': "Usuario",
                'perfil': perfil,
                'form_m' : form_m,
                'usuario': usuario,
                }
    formato =  request.GET.get('format')
    if formato == 'xml':
        return render(request, 'miscosas/usuario.xml', context, content_type='text/xml')
    elif formato == 'json':
        context = [ perfil,
                    ]
        contenidos = serializers.serialize('json', context)
        return HttpResponse(contenidos, content_type="application/json")
    return render(request, 'miscosas/usuario.html', context)


def info(request):
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
    else:
        perfil = None
    context = { 'titulo': "Información",
                'perfil': perfil,
                }
    return render(request, 'miscosas/info.html', context)


def logout_view(request):
    logout(request)
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario = request.user)
    else:
        perfil = None
    context = { 'titulo': "logout",
                'perfil': perfil,
                }
    return render(request, 'miscosas/loggedin.html', context)

def signup(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            perfil = Perfil(usuario = user)
            perfil.save()
        return redirect("/miscosas/")
    else:
        form = UserForm()
        context = {'titulo': "inicio Sesión",
                    'form': form}
        return render(request, 'miscosas/signup.html', context)
