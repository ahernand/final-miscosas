from django.urls import path
from . import views

urlpatterns = [
    path('logout/', views.logout_view),
    path('signup', views.signup),
    path('usuarios/', views.users_view),
    path('usuarios/<string>/', views.one_user),
    path('alimentadores/', views.alimentadores),
    path('alimentadores/<string>/', views.aliment),
    path('item/<string>/', views.item),
    path('', views.index),
    path('info/', views.info),
]
