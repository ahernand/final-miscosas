from django.contrib import admin
from .models import Item, Alimentador, Perfil, Item_votados, Comentario

admin.site.register(Alimentador)
admin.site.register(Item)
admin.site.register(Comentario)
admin.site.register(Perfil)
admin.site.register(Item_votados)
