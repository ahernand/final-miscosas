from django.test import TestCase
from django.test import TestCase, Client
from django.utils import timezone
from . import views
from . import models
from django.contrib.auth.models import User
from .models import Perfil, Alimentador, Item, Item_votados, Comentario

# Create your tests here.

class TestViews(TestCase):

    #Crear distintas bases de datos
    def setUp(self):
        self.user1 = models.User.objects.create(email = "prueba1@prueba.com", username = "ainhoa1")
        self.user2 = models.User.objects.create(email = "prueba2@prueba.com", username = "ainhoa2")
        self.perfil = models.Perfil.objects.create(usuario = self.user1)
        self.perfil2 = models.Perfil.objects.create(usuario = self.user2)
        self.aliment = models.Alimentador.objects.create(id_channel ="The+Beatles", titulo = "The Beatles",
                enlace = "https://www.last.fm/music/The+Beatles")
        self.it = models.Item.objects.create(alimentador = self.aliment, id_item = "Abbey+Road", titulo = "Abbey Road",
                enlace = 'https://www.last.fm/music/The+Beatles/Abbey+Road',
                    description = 'no photo')
        self.it_voto = models.Item_votados.objects.create(usuario = self.user1, item = self.it, voto = 1)
        self.comentario = models.Comentario.objects.create(usuario = self.perfil, item = self.it,
                        cuerpo = "hola", fecha = timezone.now())

    #Pruebas Index
    def test_index_ok(self):
        response = self.client.get('/miscosas/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.index)

    #Ver si la pagina de alimentadores contiene el alimentador escrito en la base de datos
    def test_get_alimentadores_ok(self):
        response = self.client.get('/miscosas/alimentadores/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.aliment.id_channel)

    def test_get_usuario_ok(self):
        response = self.client.get('/miscosas/usuarios/' + self.user1.username + '/')
        self.assertContains(response, self.user1.username)
        self.assertEqual(response.resolver_match.func, views.one_user)

    #Ver si la pagina de usuarios contiene el perfil de user 2
    def test_get_usuarios_ok(self):
        response = self.client.get('/miscosas/usuarios/')
        self.assertContains(response, self.perfil2.usuario.username)

    #Ver si la pagina de información contiene parte del texto de su respectiva plantilla
    def test_get_info_ok(self):
        response = self.client.get('/miscosas/info/')
        self.assertContains(response, "La aplicación web")
        self.assertEqual(response.resolver_match.func, views.info)

    #Test de Prueba Fallo URL
    def test_404_url(self):
        response = self.client.get('/prueba/')
        self.assertEqual(response.status_code, 404)

    #Hacer un POST en alimentador Youtube
    def test_get_alimentador_YT_view(self):
        prueba = "UC300utwSVAYOoRLEqmsprfg"
        formulario = {'aliment':'LastFM',
                        'id' : prueba,
                    }
        response = self.client.post('/miscosas/alimentadores/' + prueba + '/', formulario)
        self.assertEqual(response.resolver_match.func, views.aliment)

    def test_get_alimentador_lastFM_view(self):
        prueba = "The Beatles"
        formulario = {'aliment':'LastFM',
                        'id' : prueba,
                        'submit' : "Deseleccionar"
                    }
        prueba = prueba.replace(' ', '+')
        response = self.client.post('/miscosas/alimentadores/' + prueba + '/', formulario)
        self.assertEqual(response.status_code, 200)

    def test_get_alimentador_lastFM_view(self):
        formulario = {
                        "action": "",
                        "submit" :"Seleccionar",
                    }
        response = self.client.post('/miscosas/alimentadores/' + self.aliment.id_channel + '/', formulario)
        self.assertTrue(self.aliment.select)

    #Hacer un POST en alimentador Reddit
    def test_get_alimentador_Reddit_view(self):
        prueba = "r/memes"
        formulario = {'aliment':'Reddit',
                        'id' : prueba,
                    }
        prueba = prueba.replace('/', '_')
        response = self.client.post('/miscosas/alimentadores/', formulario)
        self.assertEqual(response.resolver_match.func, views.alimentadores)

    #Definir un alimentador no válido
    def aliment_no_valido(self):
        prueba = "Error"
        formulario = {'aliment':'LastFM',
                        'id' : prueba,
                        'submit' : "Deseleccionar"
                    }
        prueba = prueba.replace(' ', '+')
        response = self.client.post('/miscosas/alimentadores/' + prueba + '/', formulario)
        self.assertEqual(response.status_code, 404)


    def test_item_view(self):
        response = self.client.get('/miscosas/item/' + self.it.id_item + '/')
        self.assertEqual(response.resolver_match.func, views.item)
        #Comprobar que el item contiene el comentario
        self.assertContains(response, self.comentario.cuerpo)


class UserModelTest(TestCase):

    def test_user_creation(self):
        user = User(email = "prueba@prueba.com", username='prueba_user')
        user.save()
        users = User.objects.all()
        self.assertEquals(users.count(), 1)
        self.assertEquals(user.email, "prueba@prueba.com")
        self.assertEquals(user.username, "prueba_user")

    def test_perfil_creation(self):
        usuario = User(email = "prueba@prueba.com", username='prueba_user')
        usuario.save()
        perfil = Perfil(usuario = usuario)
        perfil.save()
        self.assertEquals(perfil.usuario.email, "prueba@prueba.com")
        self.assertEquals(perfil.usuario.username, "prueba_user")
        self.assertEquals(perfil.foto, "/miscosas/images/foto.jpg")
        self.assertEquals(perfil.modo_pagina, "l")
        self.assertEquals(perfil.tamano_letra, "m")

class AlimentadorModelTest(TestCase):

    def test_aliment_creation(self):
        aliment = Alimentador(id_channel ="The+Beatles", titulo = "The Beatles",
                enlace = "https://www.last.fm/music/The+Beatles")
        aliment.save()
        aliments = Alimentador.objects.all()
        self.assertEquals(aliments.count(), 1)
        self.assertEquals(aliment.id_channel, "The+Beatles")
        self.assertEquals(aliment.enlace, "https://www.last.fm/music/The+Beatles")
        self.assertEquals(aliment.titulo, "The Beatles")

    def test_item_creation(self):
        aliment = Alimentador(id_channel ="The+Beatles", titulo = "The Beatles",
                enlace = "https://www.last.fm/music/The+Beatles")
        aliment.save()
        it = Item(alimentador = aliment, id_item = "Abbey+Road", titulo = "Abbey Road",
                enlace = 'https://www.last.fm/music/The+Beatles/Abbey+Road',
                    description = 'no photo')
        it.save()
        self.assertEquals(it.alimentador.id_channel, "The+Beatles")
        self.assertEquals(it.id_item, "Abbey+Road")
        self.assertEquals(it.titulo, "Abbey Road")
        self.assertEquals(it.description, "no photo")
        self.assertEquals(it.enlace, "https://www.last.fm/music/The+Beatles/Abbey+Road")
