from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import  Alimentador, Item

class RedditHandler(ContentHandler):
    """Clase para:
    Obtener contenido de un documento XLM de una pagina de reddit
    """

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.id_aliment = ""
        self.id_noticia = ""
        self.nombre_aliment = ""
        self.link_aliment = ""
        self.content = ""
        self.titulo = ""
        self.link = ""
        self.descripcion = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
            try:
                aliment = Alimentador.objects.get(id_channel= self.id_channel)
            except Alimentador.DoesNotExist:
                aliment = Alimentador(id_channel = self.id_channel, titulo = self.nombre_aliment,
                            enlace = self.link_aliment)
            aliment.save()
        elif self.inEntry:
            if name == 'content':
                self.inContent = True
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'id':
                self.inContent = True
        elif not self.inEntry and name == 'title':
            self.inContent = True
        elif not self.inEntry and name == 'id':
            self.inContent = True
        elif not self.inEntry and attrs.get('rel') == "alternate" and name == 'link':
            self.link_aliment = attrs.get('href')

    def endElement (self, name):

        if name == 'entry':
            self.inEntry = False
            aliment = Alimentador.objects.get(id_channel = self.id_channel)
            try:
                item = Item.objects.get(id_item = self.id_noticia)
            except Item.DoesNotExist:
                It = Item(alimentador = aliment, id_item = self.id_noticia, titulo = self.titulo,
                    enlace = self.link, description = self.descripcion)
                It.save()

        elif self.inEntry:
            if name == 'title':
                self.titulo = self.content
                self.content = ""
                self.inContent = False
            elif name == 'content':
                self.descripcion = self.content
                self.content = ""
                self.inContent = False
            elif name == 'id':
                self.id_noticia = self.content
                self.content = ""
                self.inContent = False

        elif not self.inEntry:
            if name == 'title':
                self.nombre_aliment = self.content
                self.content = ""
                self.inContent = False
            if name == 'id':
                self.id_channel = self.content
                self.id_channel = self.id_channel.replace('/', '_')
                self.id_channel = self.id_channel[1:-4]
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class RedditChannel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = RedditHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
