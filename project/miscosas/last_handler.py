from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import  Alimentador, Item

class LastHandler(ContentHandler):
    """Clase para:
    Obtener contenido de un documento XLM de un artista de LastFM
    """

    def __init__ (self):
        
        self.inAlbum = False
        self.inContent = False
        self.inArtist = False
        self.id_channel = "" #Nombre artista editado
        self.nombre_channel = "" #Nombre artista
        self.link_channel = "" #Enlace Album
        self.content = ""
        self.id_album = "" #Para llegar al item
        self.titulo = ""    #Titulo del album
        self.link = ""      #Enlace del album
        self.imagen = ""  #La imagen de portada

    def startElement (self, name, attrs):
        if name == 'album':
            self.inAlbum = True
        elif self.inAlbum and not self.inArtist:
            if name == 'name':
                self.inContent = True
            if name == 'artist':
                self.inArtist = True
            elif name == 'url':
                self.inContent = True
            elif name == 'image':
                self.inContent = True
        elif self.inArtist:
            if name == 'name':
                self.inContent = True
            elif name == 'url':
                self.inContent = True

    def endElement (self, name):

        if name == 'album':
            self.inAlbum = False
            try:
                aliment = Alimentador.objects.get(id_channel= self.id_channel)
            except Alimentador.DoesNotExist:
                aliment = Alimentador(id_channel = self.id_channel, titulo = self.nombre_channel,
                                 enlace = self.link_channel)
                aliment.save()
            try:
                item = Item.objects.get(id_item = self.id_album, alimentador = aliment)
            except Item.DoesNotExist:
                It = Item(alimentador = aliment, id_item = self.id_album, titulo = self.titulo,
                    enlace = self.link, description = self.imagen)
                It.save()

        elif self.inAlbum and not self.inArtist:
            if name == 'name':
                self.titulo = self.content
                self.id_album = self.titulo.replace(' ', '+')
                self.content = ""
                self.inContent = False
            if name == 'url':
                self.link = self.content
                self.content = ""
                self.inContent = False
            elif name == 'image':
                self.imagen = self.content
                self.content = ""
                self.inContent = False
        elif self.inAlbum and self.inArtist:
            if name == 'name':
                self.nombre_channel = self.content
                self.id_channel = self.nombre_channel.replace(' ', '+')
                self.content = ""
                self.inContent = False
            elif name == 'url':
                self.link_channel = self.content
                self.content = ""
                self.inContent = False
                self.inArtist = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class LastChannel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = LastHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
