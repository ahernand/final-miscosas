from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import  Alimentador, Item

class YTHandler(ContentHandler):
    """Clase para:
    Obtener contenido de un documento XLM de un canal de youtube
    """

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.idContent = False
        self.id_channel = ""
        self.id_video = ""
        self.nombre_channel = ""
        self.link_channel = ""
        self.content = ""
        self.titulo = ""
        self.link = ""
        self.descripcion = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
            try:
                aliment = Alimentador.objects.get(id_channel= self.id_channel)
            except Alimentador.DoesNotExist:
                aliment = Alimentador(id_channel = self.id_channel, titulo = self.nombre_channel,
                            enlace = self.link_channel)
            aliment.save()
        elif self.inEntry:
            if name == 'yt:videoId':
                self.inContent = True
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'media:description':
                self.inContent = True
        elif not self.inEntry and name == 'yt:channelId':
            self.inContent = True
            self.idContent = True
        elif not self.inEntry and name == 'title':
            self.inContent = True
        elif not self.inEntry and self.idContent and name == 'link':
            self.link_channel = attrs.get('href')


    def endElement (self, name):

        if name == 'entry':
            self.inEntry = False
            aliment = Alimentador.objects.get(id_channel=self.id_channel)
            try:
                item = Item.objects.get(id_item=self.id_video)
            except Item.DoesNotExist:
                It = Item(alimentador = aliment, id_item = self.id_video, titulo = self.titulo, enlace = self.link,
                    description = self.descripcion)
                It.save()

        elif self.inEntry:
            if name == 'title':
                self.titulo = self.content
                self.content = ""
                self.inContent = False
            elif name == 'media:description':
                self.descripcion = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.id_video = self.content
                self.content = ""
                self.inContent = False

        elif not self.inEntry:
            if name == 'yt:channelId':
                self.id_channel = self.content
                self.content = ""
                self.inContent = False
            if name == 'title':
                self.nombre_channel = self.content
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class YTChannel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
