from django.db import models
from django.contrib.auth.models import User

class Perfil(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to='miscosas/images/', default = "/miscosas/images/foto.jpg")
    MODO = (
        ('l', 'Ligero'),
        ('o', 'Oscuro'),
    )
    modo_pagina = models.CharField(max_length=1, choices=MODO, blank=True, default='l', help_text='Modo de la pagina')
    TAMANO = (
        ('p', 'Pequeña'),
        ('m', 'Mediana'),
        ('g', 'Grande'),
    )
    tamano_letra = models.CharField(max_length=1, choices=TAMANO, blank=True, default='m', help_text='Tamaño de la letra')
    def __str__(self):
        return self.usuario.username

class Alimentador(models.Model):
    id_channel = models.CharField(max_length=200)
    titulo = models.CharField(max_length=64)
    enlace = models.URLField()
    puntuacion = models.IntegerField(default=0)
    select = models.BooleanField(default = True)
    def __str__(self):
        return self.id_channel

class Item(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE)
    id_item = models.CharField(max_length=256) #Cambiar nombre
    titulo = models.CharField(max_length=64) #Titulo
    enlace = models.URLField()
    description = models.CharField(max_length=512, default = "")
    votos_pos = models.IntegerField(default=0)
    votos_neg = models.IntegerField(default=0)

    def __str__(self):
        return self.titulo

class Comentario(models.Model):
    usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    cuerpo = models.TextField(blank=False, max_length=256)
    foto = models.ImageField(upload_to='miscosas/images/', default = "no photo")
    fecha = models.DateTimeField('publicado')
    def __str__(self):
        return self.usuario.usuario.username + " --> " + self.item.titulo


class Item_votados(models.Model):
     usuario = models.ForeignKey(User, on_delete=models.CASCADE)
     item = models.ForeignKey(Item, on_delete=models.CASCADE)
     voto = models.IntegerField(default=0)

     def __str__(self):
         return self.usuario.username + " --> " + self.item.titulo
