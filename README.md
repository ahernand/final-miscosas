# final-miscosas

Repositorio de inicio de la práctica final. Recuerda realizar una derivación (fork) de este repositorio y hacer múltiples commits al realizar tu práctica.

# Entrega practica
## Datos
* Nombre: Ainhoa Hernández Sánchez.
* Titulación: Ingeniería en tecnologías de telecomunicación + Ingeniería aeroespacial en aeronavegación.
* Despliegue (url): [Despliegue(http://ahs1010.pythonanywhere.com/miscosas/]
* Video básico (url): [Video básico(https://www.youtube.com/watch?v=7Bd33IS3XBE)]
* Video parte opcional (url): [Video parte opcional(https://www.youtube.com/watch?v=Eq5aL_WH8Tc)]
## Cuenta Admin Site
* ainhoa/12345678
## Cuentas usuarios
* dani/danielin
* pepe/pepito
* ejemplo/12345678
## Resumen parte obligatoria
* Se puede añadir un alimentador a través del formulario, o actualizarlo a través de este o co el botón en la página principal.
* Todas las páginas tienen la funcionalidad básica.
* Los usuarios se pueden registrar sin hacer uso de /admin. Mediante el cuadro de la esquina superior  derecha.
* Documento XML y JSON de la página principal.
* Se puede cambiar el diseño de la web a través de la pagina de su perfil mientras se esté loggeado.
* Si se selecciona de nuevo un voto se elimina de la base de datos, porque se entiende que el usuario quiere quitarlo.
* Se tiene un menú desde el que se puede acceder a distintos recursos de la aplicación. 
* Añadidos Test.py de nuestra práctica.
## Lista partes opcionales
* Se ha añadido favicon.
* Ofrece todas las paginas en formato JSON y XML.
* Se registran los usuarios.
* Se han añadido fotos en los comentarios.
* Se ha implementado algun test de error.
* Se ha añadido un alimentador además de los obligatorios en el enunciado.
